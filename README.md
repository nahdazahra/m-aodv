# M-AODV Routing Scheme

> **Nahda Fauziyah Zahra - 05111540000141**
> 
> Jaringan Nirkabel 2018

- [M-AODV Routing Scheme](#m-aodv-routing-scheme)
    - [Referensi](#referensi)
    - [Deskripsi M-AODV Routing Scheme](#deskripsi-m-aodv-routing-scheme)
    - [Implementasi Modifikasi](#implementasi-modifikasi)

## Referensi
- Judul     : **M-AODV: Modified Ad Hoc On-demand Distance Vector Routing Scheme**
- Publisher : **9th International Symposium on Communication Systems, Networks & Digital Sign (CSNDSP), 2014**
- Keyword   : **MANETs**; **AODV**; **M-AODV**; **performance metric**

## Deskripsi M-AODV Routing Scheme

Pada M-AODV, modifikasi yang dilakukan terletak pada *routing scheme* dengan mekanisme *routing discovery* yang berbeda dari AODV karena setiap paket **RREQ** akan di-*reply* dengan paket **RREP** oleh *destination node* dengan mempertimbangkan nilai ***throughput*** dari node setiap node yang dilaluinya. Setiap node akan diinisialisasi dengan nilai *throughput*-nya masing-masing, lalu paket **RREQ** dan **RREP** yang dikirim akan disisipkan dengan nilai throughput dari node yang dilaluinya. Nilai throughput yang tersimpan pada paket **RREQ** dan **RREP** adalah nilai throughput terkecil dari node yang dilalui. Alur *routing discovery* pada M-AODV Routing Metric sebagai berikut :

- *Source node* melakukan broadcast paket **RREQ** dan menyimpan nilai throughput dari node tersebut.
- Nilai throughput yang tersimpan pada paket **RREQ** akan dibandingkan dengan nilai throughput pada setiap node yang dilalui. Perbandingan dilakukan untuk menentukan apakah nilai throughput dari paket **RREQ** lebih kecil dari nilai throughhput node. Jika nilai throughput node lebih kecil, maka nilai throughput pada paket **RREQ** diperbarui menjadi nilai throughput node tersebut. Jika tidak, maka paket **RREQ** langsung diteruskan.
- Ketika paket **RREQ** dengan *sequence destination number* yang sama diterima lebih dari 1 kali oleh sebuah node, maka node tersebut akan membandingkan nilai throughput paket **RREQ** yang baru dengan nilai throughput paket **RREQ** sebelumnya yang disimpan pada ***routing table*** node tersebut dan akan meneruskan paket **RREQ** tersebut **hanya jika** nilai throughput paket tersebut **lebih besar** dari paket **RREQ** yang sebelumnya. Jika nilai throughput paket **RREQ** yang baru lebih kecil, maka paket **RREQ** tersebut akan di-drop.
- Ketika paket **RREQ** mencapai *destination node*, maka akan dibalas dengan paket **RREP** yang diinisialisasi dengan nilai throughput dari *destination node* tersebut.
- Nilai throughput pada paket **RREP**akan dibandingkan lagi dengan nilai throughput pada setiap node yang dilaluinya. Pembaharuan nilai yang dilakukan seperti yang dilakukan pada pengiriman paket **RREQ**.
- Jika *source node* menerima lebih dari 1 paket **RREP** dengan  *sequence destination number* yang sama, maka *source node* akan menyimpan rute dengan nilai throughput yang lebih besar.
- Pemilihan rute akan mempertimbangkan nilai ***hop count*** jika nilai throughput paket **RREP** sama dengan nilai throughput paket sebelumnya.

Tujuan dari M-AODV adalah :
- Memperbaiki *key performance metrics* pada AODV.
- Mengurangi jumlah paket RREQ yang di-*broadcast*

## Implementasi Modifikasi
1. menambahkan header throughput pada rreq header dan rrep header
2. menghitung nilai throughput setiap kali melakukan sendrequest / sendreply
3. saat forward request, akan dibandingkan nilai throughput link dengan nilai throughput route request untuk menentukan nilai throughput route request
4. menyimpan nilai throughput pada routing table
5. saat forward reply, akan dibandingkan nilai throughput link dengan nilai throughput route reply untuk menentukan nilai throughput route reply
6. membuat aodv menjadi multireply